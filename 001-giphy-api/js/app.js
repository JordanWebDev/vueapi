var searchBar = Vue.component('search-bar', {
    data(){
        return {
            criteria: null,     // a property to hold our "search critera/query"
            apiKey: '2ZYObCqX35tqtzwthqNmR1x288S8ZUgm',
            limit: 15,          // set limit as a data property so it can be easily adjusted, we could even set it as a "prop"
            gifs: [],           // empty array (our list of gifs will be an array of objects)
            dropdownOpen: false,    // dropdown closed by default
            selectedGif: ''     // a property to store our currently selected GIF
        }
    },
    template: `
        <div class="search-bar">
            <div class="input-group">
                <input type="text"
                    @keyup.enter="doSearch"
                    @focus="doFocus"
                    v-model="criteria"
                    class="form-control"
                    placeholder="Search for GIFs" />

                <div class="input-group-append">
                    <button @click="doSearch" class="btn btn-primary">Search</button>
                </div>
            </div>
            <div class="dropdown" :class="{ 'show' : dropdownOpen }">
                <div class="dropdown-menu">
                    <a
                        href="#"
                        v-for="gif in gifs"
                        class="dropdown-item"
                        @click="selectGif(gif.images)">

                        <img :src="gif.images.fixed_width.url" />
                    </a>
                </div>
            </div>
            <div v-show="selectedGif" class="card selectedGifPreview">

                <div class="card-body">
                    <img :src="selectedGif" />
                    <br />
                    <a href="#" class="btn btn-sm btn-danger" @click="selectedGif=''">x</a>

                </div>
            </div>
        </div>
    `,
    methods: {
        doSearch() {
            // if no search criteria is entered, cancel the function
            if(!this.criteria){
                 return false;
            }

            // clear gifs list and close dropdown menu when a new search is entered
            this.gifs = [];
            this.dropdownOpen = false;

            // use axios to make our AJAX call to the Giphy API Search Endpoint
            // paramaters: search query, api key, and result limit
            axios.get('http://api.giphy.com/v1/gifs/search?q=' + this.criteria + '&api_key=' + this.apiKey + '&limit=' + this.limit)
                .then((response) => {

                    // explore response in the devtools console to better understand the format of the data we receive back from the API
                    console.log(response);

                    // to loadGifs() we pass response.data.data which seems weird, but that's how Giphy has formatted their response JSON
                    // explore the console.log(response) in your dev tools and see for yourself!
                    this.loadGifs(response.data.data);
                });
        },

        loadGifs(data) {
            // set our gifs property to contain the list of GIFs we just received from the response
            this.gifs = data;

            // now that we have gifs, open the dropdown menu to see them!
            this.dropdownOpen = true;
        },

        selectGif(gif) {
            // store the path of the GIF we selected, we'll pass the path to Laravel to create GIF comments
            console.log(gif);
            this.selectedGif = gif.fixed_height.url;

            // we've made a selection, so let's close the dropdown
            this.dropdownOpen = false;
        },
        doFocus(){
            // small UX improvement -- if we re-focus on the textfield, and it isn't empty, open the dropdown menu
            if(this.criteria) this.dropdownOpen = true;
        }
    }
});

var vue = new Vue({
    el: '#app'
});
